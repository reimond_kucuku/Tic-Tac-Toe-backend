package al.reimond.tictactoe.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document(collection = "Games")
public class Game {
    @Id
    private String id;
    private String player1Name;
    private String player2Name;
    private List<Movement> movements;
    private String status;

    public Game() {
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Game(String id) {
        this(id, "Mira", "Beni");
    }

    public Game(String id, String player1Name, String player2Name, List<Movement> movements) {
        this.id = id;
        this.player1Name = player1Name;
        this.player2Name = player2Name;
        this.movements = movements;
    }



    public Game(String id, String mira, String beni) {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPlayer1Name() {
        return player1Name;
    }

    public void setPlayer1Name(String player1Name) {
        this.player1Name = player1Name;
    }

    public String getPlayer2Name() {
        return player2Name;
    }

    public void setPlayer2Name(String player2Name) {
        this.player2Name = player2Name;
    }

    public List<Movement> getMovements() {
        return movements;
    }

    public void setMovements(List<Movement> movements) {
        this.movements = movements;
    }

    public void insertMovement(Movement movement){
        if(this.movements == null){
            this.movements = List.of(movement);
        }
        else {
            this.movements.add(movement);
        }
    }

    @Override
    public String toString() {
        return "Game{" +
                "id='" + id + '\'' +
                ", player1Name='" + player1Name + '\'' +
                ", player2Name='" + player2Name + '\'' +
                ", movements=" + movements +
                '}';
    }
}
