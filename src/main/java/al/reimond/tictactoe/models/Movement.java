package al.reimond.tictactoe.models;

import java.util.Arrays;

public class Movement {
    private String player;
    private int[] positions;

    public Movement(String player, int[] positions) {
        this.player = player;
        this.positions = positions;
    }

    public String getPlayer() {
        return player;
    }

    public void setPlayer(String player) {
        this.player = player;
    }

    public int[] getPositions() {
        return positions;
    }

    public void setPositions(int[] positions) {
        this.positions = positions;
    }

    @Override
    public String toString() {
        return "Movement{" +
                "player='" + player + '\'' +
                ", positions=" + Arrays.toString(positions) +
                '}';
    }
}
