package al.reimond.tictactoe;

import al.reimond.tictactoe.models.Game;
import al.reimond.tictactoe.models.Movement;
import al.reimond.tictactoe.repo.GameRepository;
import al.reimond.tictactoe.service.GameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TicTacToeApplication implements CommandLineRunner {

    @Autowired
    private GameRepository repository;
    private GameService service;

    public static void main(String[] args) {
        SpringApplication.run(TicTacToeApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
//        repository.deleteAll();
//
//        // save a couple of customers
//        repository.save(new Game("1"));
//        repository.save(new Game("2", "Ana", "Gerti"));
//
//        // fetch all customers
//        System.out.println("Customers found with findAll():");
//        System.out.println("-------------------------------");
//        for (Game game : repository.findAll()) {
//            System.out.println(game);
//        }
//        System.out.println();
//
//        // fetch an individual customer
//        System.out.println("Customer found with findByFirstName('Alice'):");
//        System.out.println("--------------------------------");
//        System.out.println(repository.findGameById("1"));


    }
}
