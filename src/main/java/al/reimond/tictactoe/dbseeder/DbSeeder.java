package al.reimond.tictactoe.dbseeder;

import al.reimond.tictactoe.models.Game;
import al.reimond.tictactoe.models.Movement;
import al.reimond.tictactoe.repo.GameRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Component
public class DbSeeder implements CommandLineRunner {
    private final GameRepository gameRepository;

    public DbSeeder(GameRepository gameRepository) {
        this.gameRepository = gameRepository;
    }

    @Override
    public void run(String... strings) throws Exception {
//        Game game = new Game(
//                "1",
//                "Meri",
//                "Ina",
//                Arrays.asList(
//                        new Movement("1",new int []{0,1} ),
//                        new Movement("2", new int [] {1,1})
//                )
//        );
//        Game game2 = new Game(
//                "1",
//                "Berti",
//                "Ina",
//                Arrays.asList(
//                        new Movement("1",new int []{1,1} )
//                )
//        );
//        // drop all games
//        this.gameRepository.deleteAll();
//
//        // add games to the database
//        List<Game> games = Arrays.asList(game, game2);
//        this.gameRepository.saveAll(games);
    }
}
