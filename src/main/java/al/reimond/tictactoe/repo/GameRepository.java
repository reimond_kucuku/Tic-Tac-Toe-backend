package al.reimond.tictactoe.repo;

import al.reimond.tictactoe.models.Game;
import al.reimond.tictactoe.models.Movement;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.CrossOrigin;

@Repository
@CrossOrigin(origins = "http://localhost:4200")
public interface GameRepository extends MongoRepository<Game, String> {
    void deleteGamesById(String id);

}
