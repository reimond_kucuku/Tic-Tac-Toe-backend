package al.reimond.tictactoe.controller;

import al.reimond.tictactoe.models.Game;
import al.reimond.tictactoe.models.Movement;
import al.reimond.tictactoe.repo.GameRepository;
import al.reimond.tictactoe.service.GameService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/games")
public class GameController {
    private final GameService gameService;

    public GameController(GameService gameService) {
        this.gameService = gameService;
    }

    @GetMapping("/all")
    @CrossOrigin(origins = "http://localhost:4200")
    public ResponseEntity<List<Game>> getAll() {
        List<Game> games = gameService.findAllGames();
        return new ResponseEntity<>(games, HttpStatus.OK);
    }
    @GetMapping("/current")
    @CrossOrigin(origins = "http://localhost:4200")
    public ResponseEntity<List<Game>> getCurrent() {
        List<Game> unfinished = gameService.findAllGames()
                .stream()
                .filter(x -> x.getStatus().equals("ongoing"))
                .collect(Collectors.toList());
        return new ResponseEntity<>(unfinished, HttpStatus.OK);
    }

    @PostMapping(value = "/create", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @CrossOrigin(origins = "http://localhost:4200")
    public ResponseEntity<Game> createGame(@RequestBody Game game){
        Game newGame = gameService.addGame(game);
//        newGame.setStatus("ongoing");
        return new ResponseEntity<>(newGame, HttpStatus.CREATED);
    }
    @PostMapping("/add/{id}")
    @CrossOrigin(origins = "http://localhost:4200")
    public ResponseEntity<Movement> addMovement(@PathVariable("id") String id, @RequestBody Movement movement){
        gameService.addMovement(id, movement);
        return new ResponseEntity<>(movement, HttpStatus.OK);
    }

    @GetMapping("/find/{id}")
    @CrossOrigin(origins = "http://localhost:4200")
    public ResponseEntity<Game> getGame(@PathVariable("id") String id) {
        Game game = gameService.findGameById(id);
        return new ResponseEntity<>(game, HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    @CrossOrigin(origins = "http://localhost:4200")
    public ResponseEntity<?> deleteGame(@PathVariable("id") String id) {
        gameService.deleteGames(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @DeleteMapping("/delete/all")
    @CrossOrigin(origins = "http://localhost:4200")
    public ResponseEntity<?> deleteAllGames(){
        gameService.deleteAll();
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping(value = "/update/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @CrossOrigin(origins = "http://localhost:4200")
    public ResponseEntity<Game> updateGame(@PathVariable("id") String id, @RequestBody Movement movement) {
        gameService.setMovement(id, movement);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping("/finish/{id}")
    public ResponseEntity<?> finishGame(@PathVariable("id") String id) {
        gameService.endGame(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }


//    @PostMapping("/add")
//    public ResponseEntity<Movement> addMovement(@RequestBody Movement movement, String id) {
//        Game game = this.gameService.findGameById(id);
//        gameService.insertMovement(id, movement);
//        gameService.
//        return new ResponseEntity<>(movement, HttpStatus.CREATED);
//    }
}
